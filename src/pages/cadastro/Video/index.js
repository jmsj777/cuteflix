import React, { useEffect, useState } from "react"
import { Link, useHistory } from "react-router-dom"
import PageDefault from "../../../components/PageDefault"
import useForm from "../../../hooks/useForm"
import FormField from "../../../components/FormField"
import Button from "../../../components/Button"
import videosRepository from "../../../repositories/videos"
import categoriasRepository from "../../../repositories/categorias"

function CadastroVideo() {
  const history = useHistory()
  const [categorias, setCategorias] = useState([])
  const categoryTitles = categorias.map(({ titulo }) => titulo)

  const { handleChange, values } = useForm({
    titulo: "Video padrao",
    url: "https://youtu.be/hhQ3RtvmfEg",
    categoria: "Puppies",
  })

  useEffect(() => {
    async function fetchData() {
      const cats = await categoriasRepository.getAll()

      setCategorias(cats)
    }
    fetchData()
  }, [])

  return (
    <PageDefault>
      <h1>Cadastro de video</h1>

      <form
        onSubmit={(e) => {
          e.preventDefault()

          const categoriaEscolhida = categorias.find((cat) => {
            return cat.titulo === values.categoria
          })

          async function fetchData() {
            if (categoriaEscolhida) {
              const respostaCreate = await videosRepository.create({
                titulo: values.titulo,
                url: values.url,
                categoriaId: categoriaEscolhida.id,
              })
              // setDadosIniciais(categoriasComVideos)
              console.log(respostaCreate)
              history.push("/")
            } else {
              alert("categoria não existe!")
            }
          }
          fetchData().catch((err) => {
            console.log(err.message)
          })
        }}
      >
        <FormField
          label="Titulo do video"
          value={values.titulo}
          name="titulo"
          onChange={handleChange}
        />
        <FormField
          label="URL"
          value={values.url}
          name="url"
          onChange={handleChange}
        />
        <FormField
          label="Categoria"
          value={values.categoria}
          name="categoria"
          onChange={handleChange}
          suggestions={categoryTitles}
        />
        <Button type="submit">Cadastrar</Button>
      </form>

      <Link to="/cadastro/categoria">Cadastrar Categoria</Link>
    </PageDefault>
  )
}
export default CadastroVideo
