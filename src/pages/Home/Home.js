import React, { useEffect, useState } from "react"
import BannerMain from "../../components/BannerMain"
import Carousel from "../../components/Carousel"
import PageDefault from "../../components/PageDefault"

import categoriasRepository from "../../repositories/categorias"

function Home() {
  const [dadosIniciais, setDadosIniciais] = useState([])
  const mainCategory = 1

  useEffect(() => {
    async function fetchData() {
      const categoriasComVideos = await categoriasRepository.getAllWithVideos()
      setDadosIniciais(categoriasComVideos)
    }
    fetchData().catch((err) => {
      console.log(err.message)
    })
  }, [])

  return (
    <PageDefault paddingAll="0">
      {/* <Menu /> */}

      {dadosIniciais.length === 0 ? (
        <div> Loading... </div>
      ) : (
        <BannerMain
          videoTitle={dadosIniciais[mainCategory].videos[0].titulo}
          url={dadosIniciais[mainCategory].videos[0].url}
          videoDescription="A very cute video!"
        />
      )}

      {dadosIniciais.map((cat, i) => {
        if (i === mainCategory) {
          return <Carousel key={cat.id} ignoreFirstVideo category={cat} />
        }
        return <Carousel key={cat.id} category={cat} />
      })}
    </PageDefault>
  )
}

export default Home
